
public interface Unit {
	
	public double convertTo(double amt, Unit unit);
	
	public double getValue();
	
	public String toString();
}
