import javax.swing.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ConverterUI extends JFrame {
	// attributes for graphical components
	private JButton convertButton;
	private JButton clearButton;
	private JTextField unitField;
	private JTextField resultField;
	private JComboBox unit1Box;
	private JComboBox unit2Box;
	private JLabel label1;
	private UnitConverter unitconverter;

	public ConverterUI(UnitConverter uc) {
		this.unitconverter = uc;

		this.setTitle("Length Converter");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		initComponents();
	}

	/**
	 * initialize components in the window
	 */
	private void initComponents() {
		// TODO create components for the UI and position them using layout
		// manager.

		Container contents = this.getContentPane();
		LayoutManager layout = new FlowLayout();
		contents.setLayout(layout);
		convertButton = new JButton("Convert");
		clearButton = new JButton("Clear");
		// TODO add components for labels and JComboBoxes
		unitField = new JTextField(10);
		unit1Box = new JComboBox<Unit>(unitconverter.getUnits());
		label1 = new JLabel("=");
		resultField = new JTextField(10);
		unit2Box = new JComboBox<Unit>(unitconverter.getUnits());
		contents.add(unitField);
		contents.add(unit1Box);
		contents.add(label1);
		contents.add(resultField);
		contents.add(unit2Box);
		contents.add(convertButton);
		contents.add(clearButton);
		resultField.setEditable(false);
		ActionListener listener = new ConvertButtonListener();
		convertButton.addActionListener(listener);
		ActionListener listener2 = new ClearButtonListener();
		clearButton.addActionListener(listener2);
		unitField.addActionListener(listener);
		this.pack(); // resize the Frame to match size of components
	}

	class ConvertButtonListener implements ActionListener {
		/** method to perform action when the button is pressed */
		public void actionPerformed(ActionEvent evt) {
			String s =
					unitField.getText().trim();
			if
			( s.length() > 0 )
			{
				try{
					resultField.setText("" + unitconverter.convert(
							Double.valueOf(unitField.getText()),
							(Unit) unit1Box.getSelectedItem(),
							(Unit) unit2Box.getSelectedItem()));

				} catch (NumberFormatException e) {
					JOptionPane.showMessageDialog(null, "Invalid input.");
					
				}
			}
		}
	}

	// end of the inner class for ConvertButtonListener

	class ClearButtonListener implements ActionListener {

		public void actionPerformed(ActionEvent evt) {
			unitField.setText("");
			resultField.setText("");
		}
	}

	public void run(){
		this.setVisible(true);
	}

}