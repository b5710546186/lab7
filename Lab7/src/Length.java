
public enum Length implements Unit{

	METER("meter", 1.00),
	KILOMETER("kilometer", 1000.0),
	MILE("mile", 1609.344),
	FOOT("foot", 0.30480),
	WA("wa", 2.0);
	private String name;
	private double value;

	Length(String name, double value){
		this.name = name;
		this.value = value;
	}

	public double convertTo(double amount, Unit unit){
		double temp = value*amount/unit.getValue();
		return temp;
	}

	public double getValue(){
		return this.value;
	}

	public String toString(){
		return this.name;
	}
}
